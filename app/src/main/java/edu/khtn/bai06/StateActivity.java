package edu.khtn.bai06;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

public class StateActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state);
    }

    public void thongBao(String noiDung, boolean exitOrNot){
        AlertDialog.Builder msg = new AlertDialog.Builder(this, MODE_PRIVATE);
        msg.setTitle("Thông Báo!");
        msg.setMessage(noiDung);
        if (exitOrNot==true){
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }else{
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        msg.show();
    }
}
