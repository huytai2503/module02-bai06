package edu.khtn.bai06;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity implements View.OnClickListener{
    EditText nameEdit, passEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        nameEdit = (EditText) findViewById(R.id.name_edit);
        passEdit = (EditText) findViewById(R.id.pass_edit);
        Button loginBtn = (Button) findViewById(R.id.login_button);
        Button signUpBtn = (Button) findViewById(R.id.signUp_button);
        Button deleteBtn = (Button) findViewById(R.id.delete_button);
        signUpBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        deleteBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_button:
                login();
                break;
            case R.id.signUp_button:
                signup();
                break;
            case R.id.delete_button:
                delete();
                break;
            default:
                break;
        }
    }

    private void delete() {
        SharedPreferences user = getSharedPreferences
                ("user", MODE_PRIVATE);
        SharedPreferences.Editor edit = user.edit();
        edit.remove("name");
        edit.remove("pass");
        edit.commit();
        Toast.makeText(getBaseContext(), "Tài khoản đã được xóa",
                Toast.LENGTH_SHORT).show();
    }

    public void login(){
        Intent intentMain = new Intent(this, MainActivity.class);
        SharedPreferences user = getSharedPreferences
                ("user", MODE_PRIVATE);
        String sName = user.getString("name", "false");
        String sPass = user.getString("pass", "false");
        if (nameEdit.getText().toString().equalsIgnoreCase(sName) &&
                passEdit.getText().toString().equalsIgnoreCase(sPass)) {
            SharedPreferences login = getSharedPreferences
                    ("login", MODE_PRIVATE);
            SharedPreferences.Editor edit = login.edit();
            edit.putString("name", nameEdit.getText().toString());
            edit.putString("pass", passEdit.getText().toString());
            edit.commit();
            Toast.makeText(getBaseContext(), "Đăng nhập thành công",
                    Toast.LENGTH_SHORT).show();
            startActivity(intentMain);
            finish();
        } else
            Toast.makeText(getBaseContext(), "Tên và mật khẩu không đúng",
                    Toast.LENGTH_SHORT).show();
    }

    public void signup(){
        String sName = nameEdit.getText().toString();
        String sPass = passEdit.getText().toString();
        if (sName.isEmpty()||sPass.isEmpty())
            Toast.makeText(getBaseContext(), "Chưa nhập tên hoặc mật khẩu",
                    Toast.LENGTH_SHORT).show();
        else {
            SharedPreferences user = getSharedPreferences
                    ("user", MODE_PRIVATE);
            SharedPreferences.Editor edit = user.edit();
            edit.putString("name", sName);
            edit.putString("pass", sPass);
            edit.commit();
            Toast.makeText(getBaseContext(), "Tài khoản đã được tạo",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
