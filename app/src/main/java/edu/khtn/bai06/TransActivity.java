package edu.khtn.bai06;

import android.app.Activity;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class TransActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans);
        final ImageView image = (ImageView) findViewById(R.id.imageLT);
        final TransitionDrawable trans = (TransitionDrawable) image.getBackground();
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trans.getLevel() == 1)
                    trans.startTransition(50);
                trans.reverseTransition(50);
            }
        });
    }
}
