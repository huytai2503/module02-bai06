package edu.khtn.bai06;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;

public class FilesActivity extends Activity {
    LinearLayout filesLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files);
        filesLayout = (LinearLayout) findViewById(R.id.filesLayout);
        setNameButtonAndAddToLayout();
    }

    private void setNameButtonAndAddToLayout(){
        File[] arrFiles = getExternalStorage();
        for (File file :arrFiles) {
            Button button = createButton(file.getName());
            filesLayout.addView(button);
        }
    }

    private Button createButton(final String name){
        LinearLayout.LayoutParams buttonType = new LinearLayout.LayoutParams
                (300, LinearLayout.LayoutParams.WRAP_CONTENT);
        Button button = new Button(this);
        button.setText(name);
        button.setLayoutParams(buttonType);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String buttonName = name;
                Toast.makeText(getBaseContext(),buttonName,Toast.LENGTH_SHORT).show();
            }
        });
        return button;
    }

    private File[] getExternalStorage() {
        if (isStorageReadable()) {
            File externalStorage = Environment.getExternalStorageDirectory();
            File[] arrFiles = externalStorage.listFiles();
            return arrFiles;
        } else
            return null;
    }

    private boolean isStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
            return true;
        return false;
    }

    private boolean isStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
            return true;
        return false;
    }
}
