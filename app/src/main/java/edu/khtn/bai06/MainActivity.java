package edu.khtn.bai06;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity implements View.OnClickListener {
    Button btnLevel, btnState, btnLayer, btnInset, btnTrans, btnClip, btnScale,
            btnCamera, btnImage, btnCall, btnDial, btnSMS, btnMP3, btnLogout,
            btnDownload, btnFilesView;
    EditText editImage, editCall, editDial, editSmsTel, editSmsBody, editMp3;
    ImageView photoView;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static String currentPhotoPath = new String();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences login = getSharedPreferences("login", MODE_PRIVATE);
        SharedPreferences user = getSharedPreferences("user", MODE_PRIVATE);
        String sName = login.getString("name", "not");
        String sPass = login.getString("pass","not");
        if (sName.equalsIgnoreCase(user.getString("name","no")) &&
                sPass.equalsIgnoreCase(user.getString("pass","no"))) {
            Toast.makeText(getBaseContext(), "Xin Chào "+sName, Toast.LENGTH_SHORT).show();
            linkView();
            setOnclick();
        }else {
            logout();
        }
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data) {
        if (resultCode==Activity.RESULT_OK && requestCode==REQUEST_IMAGE_CAPTURE){
            Drawable drawable = Drawable.createFromPath(currentPhotoPath);
            photoView.setImageDrawable(drawable);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fileView_button:
                imIntent(FilesActivity.class);
                break;
            case R.id.download_button:
                download();
                break;
            case R.id.logout_button:
                logout();
                break;
            case R.id.stateDrawable:
                imIntent(StateActivity.class);
                break;
            case R.id.levelDrawable:
                imIntent(LevelActivity.class);
                break;
            case R.id.clipDrawable:
                imIntent(ClipActivity.class);
                break;
            case R.id.insetDrawable:
                imIntent(InsetActivity.class);
                break;
            case R.id.layerDrawable:
                imIntent(LayerActivity.class);
                break;
            case R.id.transDrawable:
                imIntent(TransActivity.class);
                break;
            case R.id.scaleDrawable:
                imIntent(ScaleActivity.class);
                break;
            case R.id.intent_camera:
                cameraIntent();
                break;
            case R.id.intent_image:
                imageIntent();
                break;
            case R.id.intent_call:
                callIntent();
                break;
            case R.id.intent_dial:
                dialIntent();
                break;
            case R.id.intent_sms:
                smsIntent();
                break;
            case R.id.intent_mp3:
                mp3Intent();
                break;
            default:
                break;
        }
    }

    private void download() {
        ConnectivityManager connectMgr = (ConnectivityManager)
                getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()){
            Toast.makeText(getBaseContext(),"Đã download",Toast.LENGTH_SHORT).show();
        }else {
            alertDialog("Thông báo!","Không có kết nối, cài đặt wifi?", false);
        }
    }

    private void mp3Intent() {
        File musicDir = Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_MUSIC);
        File mp3File = new File(musicDir + "/" + editMp3.getText() + "");
        Intent intentMP3 = new Intent(Intent.ACTION_VIEW);
        intentMP3.setDataAndType(Uri.fromFile(mp3File), "audio/*");
        if (intentMP3.resolveActivity(getPackageManager()) != null)
            startActivity(intentMP3);
    }

    private void smsIntent() {
        Intent intentSMS = new Intent(Intent.ACTION_SENDTO);
        intentSMS.setData(Uri.parse("sms:" + editSmsTel.getText() + ""));
        intentSMS.putExtra("sms_body", editSmsBody.getText() + "");
        if (intentSMS.resolveActivity(getPackageManager()) != null)
            startActivity(intentSMS);
    }

    private void dialIntent() {
        Intent intentDial = new Intent(Intent.ACTION_DIAL);
        intentDial.setData(Uri.parse("tel:" + editDial.getText() + ""));
        if (intentDial.resolveActivity(getPackageManager()) != null)
            startActivity(intentDial);
    }

    private void callIntent() {
        Intent intentCall = new Intent(Intent.ACTION_CALL);
        intentCall.setData(Uri.parse("tel:" + editCall.getText() + ""));
        if (intentCall.resolveActivity(getPackageManager()) != null)
            startActivity(intentCall);
    }

    private void imageIntent() {
        File imageDir = Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_PICTURES);
        File imageFile = new File(imageDir+"/"+editImage.getText()+"");
        Intent intentImage = new Intent(Intent.ACTION_VIEW);
        intentImage.setDataAndType(Uri.fromFile(imageFile), "image/*");
        if (intentImage.resolveActivity(getPackageManager()) != null)
            startActivity(intentImage);
    }

    private void cameraIntent() {
        try {
            Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(createImageFile()));
            if (intentCamera.resolveActivity(getPackageManager()) != null)
                startActivityForResult(intentCamera, REQUEST_IMAGE_CAPTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void imIntent(Class activity) {
        Intent intent = new Intent(this, activity);
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivity(intent);
    }

    public File createImageFile() throws Exception{
        File storageDir = Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_PICTURES);
        String fileName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File image = File.createTempFile(fileName, ".jpg", storageDir);
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void logout(){
        Intent intentLogin = new Intent(this, LoginActivity.class);
        SharedPreferences login = getSharedPreferences("login", MODE_PRIVATE);
        SharedPreferences.Editor edit = login.edit();
        edit.remove("name");
        edit.remove("pass");
        edit.commit();
        Toast.makeText(getBaseContext(),"Đã Logout", Toast.LENGTH_SHORT).show();
        finish();
        startActivity(intentLogin);
    }

    public void linkView() {
        photoView = (ImageView) findViewById(R.id.imageView);
        editImage = (EditText) findViewById(R.id.edit_image);
        editCall = (EditText) findViewById(R.id.edit_call);
        editDial = (EditText) findViewById(R.id.edit_dial);
        editSmsTel = (EditText) findViewById(R.id.edit_tel_sms);
        editSmsBody = (EditText) findViewById(R.id.edit_body_sms);
        editMp3 = (EditText) findViewById(R.id.edit_mp3);
        btnFilesView = (Button) findViewById(R.id.fileView_button);
        btnDownload = (Button) findViewById(R.id.download_button);
        btnLogout = (Button) findViewById(R.id.logout_button);
        btnLevel = (Button) findViewById(R.id.levelDrawable);
        btnState = (Button) findViewById(R.id.stateDrawable);
        btnClip = (Button) findViewById(R.id.clipDrawable);
        btnInset = (Button) findViewById(R.id.insetDrawable);
        btnLayer = (Button) findViewById(R.id.layerDrawable);
        btnTrans = (Button) findViewById(R.id.transDrawable);
        btnScale = (Button) findViewById(R.id.scaleDrawable);
        btnCamera = (Button) findViewById(R.id.intent_camera);
        btnImage = (Button) findViewById(R.id.intent_image);
        btnCall = (Button) findViewById(R.id.intent_call);
        btnDial = (Button) findViewById(R.id.intent_dial);
        btnSMS = (Button) findViewById(R.id.intent_sms);
        btnMP3 = (Button) findViewById(R.id.intent_mp3);
    }

    private void setOnclick() {
        btnFilesView.setOnClickListener(this);
        btnDownload.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnLevel.setOnClickListener(this);
        btnState.setOnClickListener(this);
        btnClip.setOnClickListener(this);
        btnInset.setOnClickListener(this);
        btnLayer.setOnClickListener(this);
        btnTrans.setOnClickListener(this);
        btnScale.setOnClickListener(this);
        btnCamera.setOnClickListener(this);
        btnImage.setOnClickListener(this);
        btnCall.setOnClickListener(this);
        btnDial.setOnClickListener(this);
        btnSMS.setOnClickListener(this);
        btnMP3.setOnClickListener(this);
    }

    public void toWifiSetting(){
        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        startActivity(intent);
    }

    public void alertDialog(String title, String message, Boolean exitOrNot) {
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setTitle(title);
        msg.setMessage(message);
        if (exitOrNot.booleanValue()) {
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else {
            msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    toWifiSetting();
                }
            });
            msg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        msg.show();
    }
}