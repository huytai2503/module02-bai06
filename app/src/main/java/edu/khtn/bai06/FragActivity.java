package edu.khtn.bai06;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import edu.khtn.bai06.fragments.LoginFragment;

public class FragActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frag);

        LoginFragment login = new LoginFragment();
        FragmentManager fragManager = getFragmentManager();
        FragmentTransaction fragTrans = fragManager.beginTransaction();
        fragTrans.add(R.id.LayoutContent, login);
        fragTrans.addToBackStack("login");
        fragTrans.commit();
    }
}
