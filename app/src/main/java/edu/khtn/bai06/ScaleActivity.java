package edu.khtn.bai06;

import android.app.Activity;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.SeekBar;

public class ScaleActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scale);
        final ImageView imageLT = (ImageView) findViewById(R.id.imageLT);
        final ImageView imageRT = (ImageView) findViewById(R.id.imageRT);
        final ImageView imageLB = (ImageView) findViewById(R.id.imageLB);
        final ImageView imageRB = (ImageView) findViewById(R.id.imageRB);
        SeekBar seekBar = (SeekBar) findViewById(R.id.seek);
        seekBar.setMax(10000);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean fromUser) {
                ScaleDrawable scaleLT = (ScaleDrawable) imageLT.getBackground();
                scaleLT.setLevel(value);
                ScaleDrawable scaleRT = (ScaleDrawable) imageRT.getBackground();
                scaleRT.setLevel(value);
                ScaleDrawable scaleLB = (ScaleDrawable) imageLB.getBackground();
                scaleLB.setLevel(value);
                ScaleDrawable scaleRB = (ScaleDrawable) imageRB.getBackground();
                scaleRB.setLevel(value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}
