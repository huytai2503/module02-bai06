package edu.khtn.bai06.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import edu.khtn.bai06.R;

public class LoginFragment extends Fragment implements View.OnClickListener{
    EditText editName, editPass;
    Button btnLogin;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, null);
        editName = (EditText) view.findViewById(R.id.name_edit);
        editPass = (EditText) view.findViewById(R.id.pass_edit);
        btnLogin = (Button) view.findViewById(R.id.login_button);
        btnLogin.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_button:
                HomeFragment home = new HomeFragment();
                FragmentManager fragManager = getFragmentManager();
                FragmentTransaction fragTrans = fragManager.beginTransaction();
                fragTrans.replace(R.id.LayoutContent, home);
                fragTrans.addToBackStack("home");
                fragTrans.commit();
                break;
        }
    }
}
