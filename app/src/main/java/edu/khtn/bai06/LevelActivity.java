package edu.khtn.bai06;

import android.app.Activity;
import android.graphics.drawable.LevelListDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.SeekBar;

public class LevelActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        final ImageView image = (ImageView) findViewById(R.id.imageLT);
        SeekBar seekBar = (SeekBar) findViewById(R.id.seek);
        seekBar.setMax(5);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean fromUser) {
                LevelListDrawable level = (LevelListDrawable) image.getBackground();
                level.setLevel(value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}
